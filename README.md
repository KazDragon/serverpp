# Server++

[![License](https://img.shields.io/github/license/KazDragon/serverpp.svg)](https://en.wikipedia.org/wiki/MIT_License)
[![GitHub Releases](https://img.shields.io/github/release/KazDragon/serverpp.svg)](https://github.com/KazDragon/serverpp/releases)

[![Github Issues](https://img.shields.io/github/issues/KazDragon/serverpp.svg)](https://github.com/KazDragon/serverpp/issues)

Server++ is an implementation of a basic networking server such as might be used for the connection handling of a Telnet server.

# Requirements

Server++ requires a C++14 compiler and the Conan package manager.

# Features / Roadmap / Progress

1. [ ] TCP server and sockets.